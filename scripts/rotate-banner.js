﻿/// <reference path="jquery-1.3.2-vsdoc.js" />

function changeBanner(index) {
    $("#page-header ul .ativo").fadeOut("slow", function() {
        $(this).removeClass("ativo");
        var element = $("#page-header li").get(index);
        $(element).fadeIn("slow");
        $(element).addClass("ativo");
    });
}

var currentIndex = 0;
function rotateBanner() {
    currentIndex = (currentIndex == 2) ? 0 : currentIndex + 1;
    changeBanner(currentIndex);
}

$(document).ready(function() {
    var intervalo = window.setInterval(rotateBanner, 5000);
});
